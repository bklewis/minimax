import java.util.List;

/* 
 * A class for running tests.
 */

public class Test {
	
	/* Tests:
	 * 		- GameState.initBoard()
	 * 		- GameState.clone()
	 * 		- GameState.copy()
	 */
	public static void test_creatingGameStates() {
		Konane k = new Konane();
		k.gameState.depth = 1;
		k.computerMarker = 'X';
		k.gameState.board[5][3] = '.';
		k.gameState.moves.add(new Move(0, 0, 0, 2));
		k.gameState.moves.add(new Move(0, 2, 2, 2));
		
		k.gameState.printBoard();
		for (Move move : k.gameState.moves)
			move.print();
		
		System.out.println();
		
//		GameState gsClone = k.gameState.clone();
//		gsClone.printBoard();
//		for (Move move : gsClone.moves)
//			move.print();
//		
//		System.out.println();
		
		GameState gsCopy = k.gameState.copy();
		gsCopy.printBoard();
		for (Move move : gsCopy.moves)
			move.print();
		
		System.out.println();
	}
	
	/* Tests:
	 * - 	GameState.move()
	 */
	public static void test_move() {
		GameState s = new GameState();
		s.initBoard();
		
		/** Test board edges **/
		//GameState northEdge1 = 
	}
	
	public static void test_successors() {
		GameState s = new GameState();
		//s.initBoard();
		
		/** RENEE'S TEST ---- PASS **/
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				s.board[i][j] = GameState.EMPTY;
			}
		}
		
		s.board[2][4] = GameState.DARK;
		s.board[3][3] = GameState.DARK;
		s.board[3][5] = GameState.DARK;
		s.board[4][4] = GameState.DARK;
		
		Piece p = new Piece(4, 5);
		s.board[p.row][p.col] = GameState.LIGHT;
		
		s.printBoard();
			
		List<GameState> successors = s.generateSuccessors(GameState.LIGHT);
		
		System.out.println("\n\n---Successors---\n");
		for (GameState state : successors) {
			state.printMoves();
			state.printBoard();
		}
		
	}
	
	public static void testMinimax(){
		Konane k = new Konane();
		k.computerMarker = 'X';
		//k.gameState.board[5][3] = '.';
		
		k.gameState.printBoard();

		k.gameState.remove(new Piece(3,3));
		k.gameState.remove(new Piece(3,4));
		
		k.gameState.printBoard();
		
		k.minimax(k.gameState, true);
		
		
		GameState s = new GameState();
		s.initBoard();
		
		

	}
	
	public static void testMoveInputParsing() {
		List<Move> moves = Main.parseMoveInput("Move 1,2 3,4 5,6 7,8");
		for (Move m : moves)
			m.print();
		Move.printMoveList(moves);
	}
	
	public static void testMinimaxAB() {
		Konane k = new Konane();
		k.computerMarker = GameState.DARK;
		k.opponentMarker = GameState.LIGHT;
		
		k.gameState.nDark = 13;
		k.gameState.nLight = 12;
		
		k.gameState.depth = 1;
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				k.gameState.board[i][j] = GameState.EMPTY;
			}
		}
		
		k.gameState.board[0][0] = GameState.LIGHT;
		k.gameState.board[0][1] = GameState.DARK;
		k.gameState.board[0][2] = GameState.LIGHT;
		k.gameState.board[0][3] = GameState.DARK;
		k.gameState.board[0][4] = GameState.LIGHT;
		k.gameState.board[0][5] = GameState.DARK;
		k.gameState.board[0][6] = GameState.LIGHT;
		k.gameState.board[0][7] = GameState.DARK;
		
		k.gameState.board[1][5] = GameState.LIGHT;
		
		k.gameState.board[2][1] = GameState.DARK;
		k.gameState.board[2][6] = GameState.LIGHT;
		k.gameState.board[2][7] = GameState.DARK;
		
		k.gameState.board[3][0] = GameState.DARK;
		
		k.gameState.board[4][1] = GameState.DARK;
		
		k.gameState.board[5][7] = GameState.LIGHT;
		
		k.gameState.board[6][4] = GameState.LIGHT;
		k.gameState.board[6][7] = GameState.DARK;
		
		k.gameState.board[7][0] = GameState.DARK;
		k.gameState.board[7][1] = GameState.LIGHT;
		k.gameState.board[7][2] = GameState.DARK;
		k.gameState.board[7][3] = GameState.LIGHT;
		k.gameState.board[7][4] = GameState.DARK;
		k.gameState.board[7][5] = GameState.LIGHT;
		k.gameState.board[7][6] = GameState.DARK;
		k.gameState.board[7][7] = GameState.LIGHT;
		
		
		
		k.print();
		
		List<GameState> successors = k.gameState.generateSuccessors(k.computerMarker);
		
		System.out.println("\n\n----Successors---\n");
		
		for (GameState successor : successors) {
			successor.printMoves();
			System.out.println("Eval: " + k.staticEval(successor));
			System.out.println("# successors: " + successor.generateSuccessors(k.opponentMarker).size());
			successor.printBoard();
			System.out.print("\n\n");
		}
		
		
		System.out.println("\n\nMINIMAXAB");
		System.out.println("COMPUTER'S MOVE:");
		pbvMovePair pair = k.minimaxAB(k.gameState, k.NEG_INFINITY, k.POS_INFINITY, true);
		pair.gameState.printMoves();
		pair.gameState.printBoard();
		
		k.gameState = pair.gameState;
		k.gameState.depth = 1;
		
		List<GameState> opponentMoves = k.gameState.generateSuccessors(k.opponentMarker);
		if (opponentMoves.size() > 0) {
			System.out.println("\nOPPONENT'S MOVE:");
			k.gameState = opponentMoves.get(0);
			k.gameState.printMoves();
			k.gameState.printBoard();
			
			System.out.println("\nCOMPUTER'S MOVE:");
			k.gameState.depth = 1;
			pair = k.minimaxAB(k.gameState, k.NEG_INFINITY, k.POS_INFINITY, true);
			pair.gameState.printMoves();
			pair.gameState.printBoard();
			
			k.gameState = pair.gameState;
			k.gameState.depth = 1;
			
			opponentMoves = k.gameState.generateSuccessors(k.opponentMarker);
			if (opponentMoves.size() > 0) {
				System.out.println("\nOPPONENT'S MOVE:");
				k.gameState = opponentMoves.get(0);
				k.gameState.printMoves();
				k.gameState.printBoard();
				
				System.out.println("\nCOMPUTER'S MOVE:");
				k.gameState.depth = 1;
				pair = k.minimaxAB(k.gameState, k.NEG_INFINITY, k.POS_INFINITY, true);
				pair.gameState.printMoves();
				pair.gameState.printBoard();
				
				k.gameState = pair.gameState;
				k.gameState.depth = 1;
				
				opponentMoves = k.gameState.generateSuccessors(k.opponentMarker);
				if (opponentMoves.size() > 0) {
					System.out.println("\nOPPONENT'S MOVE:");
					k.gameState = opponentMoves.get(0);
					k.gameState.printMoves();
					k.gameState.printBoard();
					
					System.out.println("\nCOMPUTER'S MOVE:");
					k.gameState.depth = 1;
					pair = k.minimaxAB(k.gameState, k.NEG_INFINITY, k.POS_INFINITY, true);
					pair.gameState.printMoves();
					pair.gameState.printBoard();
					
					k.gameState = pair.gameState;
					k.gameState.depth = 1;
					
					opponentMoves = k.gameState.generateSuccessors(k.opponentMarker);
					if (opponentMoves.size() > 0) {
						System.out.println("\nOPPONENT'S MOVE:");
						k.gameState = opponentMoves.get(0);
						k.gameState.printMoves();
						k.gameState.printBoard();
					}
					else {
						System.out.println("\nOPPONENT HAS NO MOVES LEFT");
					}
				}
				else {
					System.out.println("\nOPPONENT HAS NO MOVES LEFT");
				}
			}
			else {
				System.out.println("\nOPPONENT HAS NO MOVES LEFT");
			}
		}
		
	}
}