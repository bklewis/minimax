import java.util.*;

public class Konane {
	
	public static final int POS_INFINITY = Integer.MAX_VALUE;
	public static final int NEG_INFINITY = Integer.MIN_VALUE;

	//board
	GameState gameState;
	char computerMarker;
	char opponentMarker;
	int DEPTH_BOUND = 6;
	
	long staticEvals = 0;
	long cutOffs = 0;
	long branchingFactor;
	long genSuccessorsCalled = 0;
	long successorsGenned = 0;
	
		
	public Konane(){
		gameState = new GameState();
		gameState.initBoard();
	}
	
	/* Function for making a series of moves.
	 * Use for making opponent's moves.
	 */
	public GameState makeMove(List<Move> moves, char player){
		ListIterator<Move> itr = moves.listIterator();
		
		GameState state = this.gameState.copy();
		Move move;
		while (itr.hasNext()) {
			if (state == null)
				return null;
			
			move = itr.next();
			Piece p = new Piece(move.rowOrig, move.colOrig);
			int d = move.getDirection();
			
			if (d == -1)
				return null;
			
			state = state.move(p, d, player);
		}
		
		return state;
	}
	
	public boolean isDepthBound(GameState state) {
		return (state.depth >= DEPTH_BOUND);
	}
	
	public int staticEval(GameState state) {
		
		staticEvals++;
		
		if (computerMarker == state.DARK)
			return state.nDark - state.nLight;
		else
			return state.nLight - state.nDark;
	}
	
	public boolean isMaxNode(GameState state) {
		return state.depth % 2 != 0;
	}
	
	public boolean isMinNode(GameState state) {
		return state.depth % 2 == 0;
	}
	
	public pbvMovePair minimax(GameState state, boolean isMaximizingMove) {
		GameState chosenOne = null;
		char player = '.';
		player = isMaximizingMove ? computerMarker : opponentMarker;
		
		List<GameState> successors = state.generateSuccessors(player);
		
		

		if (isMaximizingMove) {
			
			if (successors.size() == 0) // loss
				return new pbvMovePair(NEG_INFINITY, state.moves, state);
			
			if (isDepthBound(state))
				return new pbvMovePair(staticEval(state), state.moves, state);
			
			genSuccessorsCalled++;
			successorsGenned += successors.size();
			
			int cbv = NEG_INFINITY;
			List<Move> bestMove = null;
			
			for (GameState successor:successors){
				pbvMovePair pair = minimax(successor, false);
				int bv = pair.pbv;
				List<Move> move = pair.move;
				
				if(bv > cbv){
					cbv = bv;
					bestMove = move;
					chosenOne = successor;
				}
			}
			return new pbvMovePair(cbv, bestMove, chosenOne);
			
		}
		
		else { 
			
			if (successors.size() == 0) // win
				return new pbvMovePair(POS_INFINITY, state.moves, state);
			
			if (isDepthBound(state))
				return new pbvMovePair(staticEval(state), state.moves, state);
			
			genSuccessorsCalled++;
			successorsGenned += successors.size();

			int cbv = POS_INFINITY;
			List<Move> bestMove = null;
			
			for (GameState successor:successors){
				pbvMovePair pair = minimax(successor, true);
				int bv = pair.pbv;
				List<Move> move = pair.move;
				
				if(bv < cbv){
					cbv = bv;
					bestMove = move;
					chosenOne = successor;
				}
			}
			return new pbvMovePair(cbv, bestMove, chosenOne);
			
		}
	}
	
	public void orderSuccessors(List<GameState> successors) {
		// order successors so highest # moves checked first
		Collections.sort(successors, new Comparator<GameState>(){

			@Override
			public int compare(GameState s1, GameState s2) {
				return s2.moves.size() - s1.moves.size(); // swapped for descending order
			}
			
		}
		);
	}
	
	
	//alpha-beta pruning version of minimax
	public pbvMovePair minimaxAB(GameState state, int a, int b, boolean isMaximizingMove) {
		GameState chosenOne = null;
		char player = '.';
		player = isMaximizingMove ? computerMarker : opponentMarker;

		List<GameState> successors = state.generateSuccessors(player);

		orderSuccessors(successors);
		
		if (isMaximizingMove) {
			if (successors.size() == 0) // loss
				return new pbvMovePair(NEG_INFINITY, state.moves, state);
			
			if (isDepthBound(state))
				return new pbvMovePair(staticEval(state), state.moves, state);
			
			genSuccessorsCalled++;
			successorsGenned += successors.size();
			
			List<Move> bestMove = null;
			
			for (GameState successor:successors){
				pbvMovePair pair = minimaxAB(successor, a, b, false);
				int bv = pair.pbv;
				List<Move> move = pair.move;
				
				if(bv > a){
					a = bv;
					bestMove = move;
					chosenOne = successor;
				}
				
				//a-b cutoff
				if(a >= b){
					cutOffs++;
					return new pbvMovePair(b, bestMove, chosenOne);
				}
			}
			return new pbvMovePair(a, bestMove, chosenOne);
		}
		else {
			if (successors.size() == 0) // win
				return new pbvMovePair(POS_INFINITY, state.moves, state);
			
			if (isDepthBound(state))
				return new pbvMovePair(staticEval(state), state.moves, state);
			
			genSuccessorsCalled++;
			successorsGenned += successors.size();
			
			List<Move> bestMove = null;
			
			for (GameState successor:successors){
				pbvMovePair pair = minimaxAB(successor, a, b, true);
				int bv = pair.pbv;
				List<Move> move = pair.move;
				
				if(bv < b){
					b = bv;
					bestMove = move;
					chosenOne = successor;
				}
				
				//a-b cutoff
				if(a >= b){
					cutOffs++;
					return new pbvMovePair(a, bestMove, chosenOne);
				}
			}
			return new pbvMovePair(b, bestMove, chosenOne);
		}
	}
	
	public void print(){
		gameState.printBoard();
	}
	
	public void printMoves(List<Move> moves) {
		//////////
		System.out.print("Move from ");
		
		Iterator<Move> itr = moves.iterator();
		Move m = itr.next();
		
		m.print();
		
		while (itr.hasNext()) {
			m = itr.next();
			Piece p = Main.backTrans(m.rowDest, m.colDest);
//			Piece p = new Piece(m.rowDest, m.colDest);
			System.out.print("to <" + p.row + ", " + p.col + "> ");
		}

		System.out.println();
	}
}
