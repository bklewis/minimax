import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import java.io.*;

public class Main {

	//players
	public static final int COMPUTER_TURN = 1;
	public static final int OPPONENT_TURN = 2;
	
	public static void main(String[] args) {

		//setup
		Konane k = new Konane();
		System.out.println("Welcome to Konane!\n");
		
		//set computer and player colors
		System.out.println("Will the computer be playing dark (X) or light (O)?");
		
		//open standard input
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		//setting the computer's color
		k.computerMarker = '.';
		while(k.computerMarker == '.'){
			try{
				System.out.print("Enter 'X' for dark or 'O' for light: ");
				k.computerMarker = br.readLine().charAt(0);
			} catch(IOException ioe){
				System.out.println("That input is invalid. Please try again.");
			}
			if (k.computerMarker == 'X'){
				System.out.println("Dark (X) will be played by the computer.");
				System.out.println("Light (O) will be played by the opponent.\n");
				k.opponentMarker = 'O';
			} else if (k.computerMarker == 'O'){
				System.out.println("Dark (X) will be played by the opponent.");
				System.out.println("Light (O) will be played by the the computer.\n");
				k.opponentMarker = 'X';
			} else {
				System.out.println("That input is invalid. Please try again.\n");
				k.computerMarker = '.';
			}
		}
		
		//dark always goes first
		char currentPlayer = 'X';
		
		System.out.println("Initial Board\n");
		k.gameState.printBoard();
		System.out.println();
		
		//handle first two moves (removing only, no piece-hopping)
		for(int i = 0; i < 2; i++){
			
			//print turn
			System.out.println("\nIt's " + currentPlayer + "'s turn!\n");
			
			//computer removal
			if (k.computerMarker == currentPlayer){
				if(k.computerMarker == 'X')
					k.gameState.remove(new Piece(3,4));
				else
					k.gameState.remove(new Piece(4,4));
				k.gameState.printBoard();
			} 
			else //opponent removal
			{ 
				String input = null;
				try{
					System.out.println("\nPlease remove one token from the center square.");
					System.out.print("Enter Move: ");
					input = br.readLine();
				} catch(IOException ioe){
					System.out.println("You cannot remove that piece.");
				}
				
				//input string parsing to get move
				String[] result = input.split("[^0123456789]*");
				List<Integer> ints = new ArrayList<Integer>();
			    for (int x=0; x<result.length; x++) {
			    	if (!result[x].equals("")) {
			    		ints.add(Integer.parseInt(result[x]));
			    	}
			    }
			    
			    k.gameState.remove(translate(ints.get(0), ints.get(1)));
			    k.gameState.printBoard();
			}
			currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
		}
		
		k.gameState.nDark--;
		k.gameState.nLight--;
		
		System.out.println();
		k.gameState.printBoard();
		System.out.println();
		
		while(true){
			
			System.out.println("It's " + currentPlayer + "'s turn!\n");
			
			//computer move
			if(currentPlayer == k.computerMarker){
//				pbvMovePair pair = k.minimaxAB(k.gameState, k.NEG_INFINITY, k.POS_INFINITY, true);
				pbvMovePair pair = k.minimax(k.gameState, true);
				
				if (pair.move == null) {
					List<GameState> successors = k.gameState.generateSuccessors(k.computerMarker);
					
					if (successors.size() == 0) {
						System.out.println("Opponent wins!");
						break;
					}
					else {
						System.out.println("# successors=" + successors.size());
						break;
					}
				}
				
				k.gameState = pair.gameState;
				k.printMoves(pair.gameState.moves);
				System.out.println("PBV="+pair.pbv);
			}
			else { //opponent move
				List<GameState> successors = k.gameState.generateSuccessors(k.opponentMarker);
				if (successors.size() == 0) {
					System.out.println("Computer wins!");
					break;
				}
				
				while (true) {
					try{
						System.out.print("Enter Move: ");
						
						List<Move> moves = parseMoveInput(br.readLine());
						if (moves == null) {
							System.out.println("Invalid move");
							continue;
						}
						
						GameState newState = k.makeMove(moves, k.opponentMarker);
						if (newState == null) {
							System.out.println("Invalid move");
							continue;
						}
						
						k.gameState = newState;
						break;
						
					} catch(IOException ioe){
						System.out.println("YOU ENTERED A BAD MOVE, AND YOU SHOULD FEEL BAD.");
						continue;
					}
				}
			}
			
			System.out.println();
			k.gameState.printBoard();
			System.out.println();
			
			//take turns: switch current player
			currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
			
			//reset depth
			k.gameState.depth = 1;
			
			int x = (k.genSuccessorsCalled == 0) ? 1 : k.genSuccessorsCalled;
			
			int branchingFactor = k.successorsGenned/x;
			
			System.out.println("STATS: "
					+ "\nstaticEvals: " + k.staticEvals
					+ "\nbranchingFacor: " + branchingFactor
					+ "\ncutOffs: " + k.cutOffs);
			
		}
		System.out.println("\n" + "GAME OVER\n");
	}
	
	
	//parse input strings into readable moves
	static List<Move> parseMoveInput(String input) {
		if (input == null)
			return null;
		
		List<Move> moves = new ArrayList<Move>();
		
		String[] result = input.split("[^0123456789]*");
		List<Integer> ints = new ArrayList<Integer>();
	    for (int x=0; x<result.length; x++) {
	    	if (!result[x].equals("")) {
	    		ints.add(Integer.parseInt(result[x]));
	    	}
	    }
	    
	    if (ints.size() < 4)
	    	return null;
	    
	    if (ints.size() % 2 != 0)
	    	return null;
	    
	    Piece p1 = translate(ints.get(0), ints.get(1));
	    Piece p2 = translate(ints.get(2), ints.get(3));
	    moves.add(new Move(p1.row, p1.col, p2.row, p2.col));
	    
	    for (int i=4; i<ints.size(); i+=2) {
	    	Move m = moves.get(moves.size()-1);
	    	Piece p = translate(ints.get(i), ints.get(i+1));
	    	moves.add(new Move(m.rowDest, m.colDest, p.row, p.col));
	    }
			
		return moves;
	}
	
	//translate from internal coordinate system to official Konane coordinate system
	public static Piece translate(int row, int col){
		col = col - 1;
		row = 8 - row;
		return new Piece(row, col);
	}
	
	//translate from official Konane coordinate system to internal coordinate system
	public static Piece backTrans(int row, int col){
		col = col + 1;
		row = 8 - row;
		return new Piece(row, col);
	}

}
