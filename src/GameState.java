import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class GameState {
	
	//board positions
	public static final char EMPTY = '.';
	public static final char DARK = 'X';
	public static final char LIGHT = 'O';
	
	
	//Konane Board
	char[][] board = new char[8][8];
	int nDark = -1;
	int nLight = -1;
	List<Move> moves = new ArrayList<Move>();
	int depth = 1; // for tracking how deep we search using minimax
	
	//empty constructor
	public GameState() {}
	
	public GameState copy() { // clones everything about a GameState EXCEPT the move list, which is empty
		GameState clone = new GameState();
		
		clone.nDark = this.nDark;
		clone.nLight = this.nLight;
		clone.depth = this.depth;
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				clone.board[i][j] = this.board[i][j];
			}
		}
		
		return clone;
	}
	
	public void initBoard() {
		nDark = 32;
		nLight = 32;
		
		//initialize board
		char piece = LIGHT;
		
		for(int i = 0; i < 8; i++){
			for(int j = 0; j < 8; j++){
				board[i][j] = piece;
				if(piece == LIGHT){
					piece = DARK;
				} else {
					piece = LIGHT;
				}
			}
			if(piece == LIGHT){
				piece = DARK;
			} else {
				piece = LIGHT;
			}
		}
	}
	
	public void remove(Piece piece){
		if (board[piece.row][piece.col] == EMPTY) // no marker
			System.out.println("Error: no piece found.");
		
		board[piece.row][piece.col] = EMPTY ;
		
	}

	
	// if move is successful, return new GameState after move has been made
	// if move is unsuccessful/invalid, return null
	public GameState move(Piece piece, int direction, char player) {
		if (board[piece.row][piece.col] != player) // wrong marker
			return null;
		
		Piece opponent = null;		// position of opponent's marker
		Piece newPosition = null;	// position of where marker will jump to
		
		if (direction == Move.NORTH) {
			if (piece.row < 2) // board edge
				return null;
			
			opponent = new Piece(piece.row-1, piece.col);
			newPosition = new Piece(piece.row-2, piece.col);
		}
		
		else if (direction == Move.SOUTH) {
			if (piece.row > 5) // board edge
				return null;
			
			opponent = new Piece(piece.row+1, piece.col);
			newPosition = new Piece(piece.row+2, piece.col);
		}
		
		else if (direction == Move.EAST) {
			if (piece.col > 5) // board edge
				return null;
			
			opponent = new Piece(piece.row, piece.col+1);
			newPosition = new Piece(piece.row, piece.col+2);
		}
					
		else if (direction == Move.WEST) {
			if (piece.col < 2) // board edge
				return null;
			
			opponent = new Piece(piece.row, piece.col-1);
			newPosition = new Piece(piece.row, piece.col-2);
		}
		
		
		if (opponent != null && newPosition != null) {
		
			if (board[opponent.row][opponent.col] == EMPTY) // check opponent piece exists
				return null;
			
			if (board[newPosition.row][newPosition.col] != EMPTY) // check free space for jumping exists
				return null;
			
			GameState newState = this.copy(); // copies this, except for moves list	
			
			newState.board[opponent.row][opponent.col] = EMPTY; // opponent piece removed
			newState.board[newPosition.row][newPosition.col] = this.board[piece.row][piece.col]; // add marker to new position			
			newState.board[piece.row][piece.col] = EMPTY; // remove old marker
			
			if (board[opponent.row][opponent.col] == DARK) // decrement opponent marker count
				newState.nDark--;
			else
				newState.nLight--;
	
			newState.moves.add(new Move(piece.row, piece.col, newPosition.row, newPosition.col)); // add this move to move list of new game state
			
			return newState;
		
		}
		
		else {
			return null;
		}
	}

	
	// returns a list of GameStates, which contain the list of moves describing how to get to successor from parent
	List<GameState> generateSuccessors(char player) {
		List<GameState> successorList = new ArrayList<GameState>();
		
		for (int row=0; row<8; row++) {				// this double loop & if statement can be optimized with help of data structures
			for (int col=0; col<8; col++) {			// so that it doesn't check every spot on the board,
				if (board[row][col] == player) {	// only the one's player actually has pieces on
					successorList.addAll(generateSuccessors(player, new Piece(row, col)));
				}
			}
		}
		
		return successorList;
	}
	
	List<GameState> generateSuccessors(char player, Piece piece) {
		List<GameState> successorList = new ArrayList<GameState>();
		
		GameState successorState;
		
		successorState = move(piece, Move.NORTH, player);
		if (successorState != null) {
			
			successorState.depth++; // successors at at the next depth level in tree
			//Move move = new Move(piece.row, piece.col, piece.row-2, piece.col); // move taken to get to this state//////
			Move move = successorState.moves.get(0);
			
			//successorState.moves.add(0, move);////////
			successorList.add(successorState);
			
			// successors generated from successorState, moving the same piece that was moved to get successorState
			List<GameState> successorSuccessors = successorState.generateSuccessors(player, new Piece(piece.row-2, piece.col));/////
			for (GameState state : successorSuccessors) {
				state.moves.add(0, move); // prepend move to these multimove successors
				successorList.add(state); // add multimove successor to list of successors
			}
			
		}
		
		successorState = move(piece, Move.SOUTH, player);
		if (successorState != null) {
			
			successorState.depth++; // successors at at the next depth level in tree
			//Move move = new Move(piece.row, piece.col, piece.row+2, piece.col); // move taken to get to this state
			Move move = successorState.moves.get(0);
			
			//successorState.moves.add(0, move);
			successorList.add(successorState);
			
			// successors generated from successorState, moving the same piece that was moved to get successorState
			List<GameState> successorSuccessors = successorState.generateSuccessors(player, new Piece(piece.row+2, piece.col));
			for (GameState state : successorSuccessors) {
				state.moves.add(0, move); // prepend move to these multimove successors
				successorList.add(state); // add multimove successor to list of successors
			}
			
		}
		
		successorState = move(piece, Move.EAST, player);
		if (successorState != null) {

			successorState.depth++; // successors at at the next depth level in tree
			//Move move = new Move(piece.row, piece.col, piece.row, piece.col+2); // move taken to get to this state
			Move move = successorState.moves.get(0);
			
			//successorState.moves.add(0, move);
			successorList.add(successorState);
			
			// successors generated from successorState, moving the same piece that was moved to get successorState
			List<GameState> successorSuccessors = successorState.generateSuccessors(player, new Piece(piece.row, piece.col+2));
			for (GameState state : successorSuccessors) {
				state.moves.add(0, move); // prepend move to these multimove successors
				successorList.add(state); // add multimove successor to list of successors
			}
			
		}
		
		successorState = move(piece, Move.WEST, player);
		if (successorState != null) {
			
			successorState.depth++; // successors at at the next depth level in tree
			//Move move = new Move(piece.row, piece.col, piece.row, piece.col-2); // move taken to get to this state
			Move move = successorState.moves.get(0);
			
			//successorState.moves.add(0, move);
			successorList.add(successorState);
			
			// successors generated from successorState, moving the same piece that was moved to get successorState
			List<GameState> successorSuccessors = successorState.generateSuccessors(player, new Piece(piece.row, piece.col-2));
			for (GameState state : successorSuccessors) {
				state.moves.add(0, move); // prepend move to these multimove successors
				successorList.add(state); // add multimove successor to list of successors
			}
			
		}
		
		return successorList;
	}
	
	public void printBoard(){
		int row = 8;
		
		System.out.println("\t1 2 3 4 5 6 7 8\n");
		
		for(int i = 0; i < 8; i++){
			System.out.print(row-- + "\t");
			for(int j = 0; j < 8; j++){
				System.out.print(board[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	// Prints moves according to Konane coordinate system
	public void printMoves() {
		System.out.print("Move from ");
		
		Iterator<Move> itr = moves.iterator();
		Move m = itr.next();
		
		m.print();
		
		while (itr.hasNext()) {
			m = itr.next();
			Piece p = Main.backTrans(m.rowDest, m.colDest);
			System.out.print("to <" + p.row + ", " + p.col + "> ");
		}

		System.out.println();
	}
}
