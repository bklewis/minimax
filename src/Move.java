import java.util.Iterator;
import java.util.List;

/* A single move represents a single jump
 * from one board location to anther.
 */
public class Move {

	// directions in which a piece may move
	public static final int NORTH = 0;
	public static final int SOUTH = 1;
	public static final int EAST = 2;
	public static final int WEST = 3;
	
	int rowOrig = 0;
	int colOrig = 0;
	
	int rowDest = 0;
	int colDest = 0;
	
	public Move(int rowOrig, int colOrig, int rowDest, int colDest){
		this.rowOrig = rowOrig;
		this.colOrig = colOrig;
		this.rowDest = rowDest;
		this.colDest = colDest;
	}
	
	/*
	 *  If a move direction is valid, returns direction of the move
	 *  Else, returns -1
	 *  Valid move directions are NORTH, SOUTH, EAST, and WEST, by 2 spaces only
	 */
	public int getDirection() {
		if (colDest == colOrig && rowDest - rowOrig == -2)
			return NORTH;
		else if (colDest == colOrig && rowDest - rowOrig == 2)
			return SOUTH;
		else if (colDest - colOrig == 2 && rowDest == rowOrig)
			return EAST;
		else if (colDest - colOrig == -2 && rowDest == rowOrig)
			return WEST;

		return -1;
	}
	
	public void print() {
		Piece origP = Main.backTrans(rowOrig, colOrig);
		Piece destP = Main.backTrans(rowDest, colDest);
		System.out.print("<" + origP.row + ", " + origP.col + "> to <" + destP.row + ", " + destP.col + "> ");
	}
	
	
	public static void printMoveList(List<Move> moves) {
		System.out.print("Move from ");
		
		Iterator<Move> itr = moves.iterator();
		Move m = itr.next();
		
		m.print();
		
		while (itr.hasNext()) {
			m = itr.next();
			Piece destP = Main.backTrans(m.rowDest, m.colDest);
			System.out.print("to <" + destP.row + ", " + destP.col + "> ");
		}

		System.out.println();
	}
	
}
