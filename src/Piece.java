// A class to hold an (x,y) coordinate on the board
// where a piece we are trying to track is located

public class Piece {
	int row = -1;
	int col = -1;
	
	public Piece(int row, int col) {
		this.row = row;
		this.col = col;
	}
}
